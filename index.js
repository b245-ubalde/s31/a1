const http = require('http');

const port = 3000;

const server = http.createServer((request, response) => {

	if(request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Hi! Welcome to the Login Page.");
	} else{
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Sorry. Page currently not available.");
	}

})

server.listen(port);

console.log(`Server is successfully running.`);